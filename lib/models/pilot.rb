class Pilot
  attr_accessor :id, :name, :last_lap, :position, :match_time, :best_lap, :average_speed

  def initialize(id, name)
    @id = id
    @name = name
    @match_time = 0
    @average_speed = 0
  end
end
