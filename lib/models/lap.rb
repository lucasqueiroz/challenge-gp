class Lap
  attr_accessor :time, :pilot, :lap_number, :lap_time, :average_lap_speed

  def initialize(params)
    @time = params[:time]
    @pilot = params[:pilot]
    @lap_number = params[:lap_number]
    @lap_time = params[:lap_time]
    @average_lap_speed = params[:average_lap_speed].to_f
  end
end
