class LapParser
  attr_reader :laps

  def initialize(input)
    @input = input
    @laps = []
    @pilots = {}
  end

  def parse
    @input.lines[1..].each do |line|
      parse_line(line)
    end
    @laps
  end

  private

  def parse_line(line)
    lap = {}
    line, lap = parse_time(line, lap)
    line, lap = parse_pilot(line, lap)
    line.lstrip!
    lap = parse_lap(line, lap)
    @laps << Lap.new(lap)
  end

  def parse_time(line, lap)
    lap[:time] = line.split(' ').first
    line.gsub!(lap[:time], '')
    [line, lap]
  end

  def parse_pilot(line, lap)
    pilot_info = line.match(/[0-9]{3} – [a-zA-Z.]+/)[0].split(' ')
    pilot_id = pilot_info.first.to_i
    pilot_name = pilot_info.last
    pilot = @pilots[pilot_id] || Pilot.new(pilot_id, pilot_name)
    @pilots[pilot.id] = pilot unless @pilots[pilot.id]
    lap[:pilot] = pilot
    line.gsub!(pilot_info.join(' '), '')
    [line, lap]
  end

  def parse_lap(line, lap)
    lap[:lap_number] = line.chars.first.to_i
    line = line[1..]
    lap_time = line.match(/\d:\d{2}\.\d{3}/)[0]
    lap[:lap_time] = ChronicDuration.parse(lap_time).round(2)
    line.strip!.gsub!(lap_time, '')
    lap[:average_lap_speed] = line.lstrip.sub(',', '.')
    lap
  end
end
