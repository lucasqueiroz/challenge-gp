#!/usr/bin/env ruby

require 'chronic_duration'
require 'terminal-table'

Dir["./lib/models/**/*.rb"].sort.each { |f| require f }

require_relative 'lap_parser'
require_relative 'pilot_formatter'

include PilotFormatter

input = File.read('./resources/input.txt')
lap_parser = LapParser.new(input)
laps = lap_parser.parse
best_lap, pilots = PilotFormatter.format_pilots(laps)
headings = [
  'Posição Chegada',
  'Código Piloto',
  'Nome Piloto',
  'Qtde Voltas Completadas',
  'Tempo Total de Prova',
  'Melhor volta',
  'Média de velocidade'
]
rows = []
pilots.sort_by(&:position).each do |pilot|
  rows << [
    pilot.position,
    pilot.id,
    pilot.name,
    pilot.last_lap,
    ChronicDuration.output(pilot.match_time, format: :chrono),
    ChronicDuration.output(pilot.best_lap, format: :chrono),
    pilot.average_speed.round(3)
  ]
end
puts Terminal::Table.new(headings: headings, rows: rows)
formatted_best_lap = ChronicDuration.output(best_lap.lap_time, format: :chrono)
puts "Melhor volta da prova: #{formatted_best_lap}"
