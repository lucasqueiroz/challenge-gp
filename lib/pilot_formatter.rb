module PilotFormatter
  def format_pilots(laps)
    laps = sort_laps_by_time(laps)
    pilots = pilots(laps)
    format_match_time(laps)
    format_positions(pilots)
    format_last_lap(laps)
    find_best_lap_by_pilot(laps)
    find_average_speed(laps)
    best_lap = find_best_lap(laps)
    [best_lap, pilots]
  end

  private

  def sort_laps_by_time(laps)
    laps.sort_by { |lap| lap.time }
  end

  def pilots(laps)
    laps.map(&:pilot).uniq { |pilot| pilot.id }
  end

  def format_match_time(laps)
    laps.each do |lap|
      lap.pilot.match_time += lap.lap_time
      lap.pilot.match_time = lap.pilot.match_time.round(2)
    end
  end

  def format_positions(pilots)
    pilots.sort_by(&:match_time).each_with_index do |pilot, index|
      pilot.position = index + 1
    end
  end

  def format_last_lap(laps)
    laps.sort_by(&:lap_number).reverse.each do |lap|
      lap.pilot.last_lap ||= lap.lap_number
    end
  end

  def find_best_lap(laps)
    laps.sort_by(&:lap_time).first
  end

  def find_best_lap_by_pilot(laps)
    laps.each do |lap|
      lap.pilot.best_lap ||= lap.lap_time
      lap.pilot.best_lap = lap.lap_time if lap.lap_time < lap.pilot.best_lap
    end
  end

  def find_average_speed(laps)
    laps.each do |lap|
      lap.pilot.average_speed += (lap.average_lap_speed / lap.pilot.last_lap.to_f)
    end
  end
end
