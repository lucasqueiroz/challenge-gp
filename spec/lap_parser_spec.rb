require 'spec_helper'

RSpec.describe LapParser do
  describe '#parse' do
    let(:input) do
      "Hora                               Piloto             Nº Volta   Tempo Volta       Velocidade média da volta\n" \
      "23:49:08.277      038 – F.MASSA                           1		1:02.852                        44,275"
    end
    let(:subject) { LapParser.new(input) }

    before do
      subject.parse
    end

    context 'when parsing valid lines' do
      let(:lap) { subject.laps.first }

      it 'has correct number of laps' do
        expect(subject.laps.size).to eq(1)
      end

      it 'has correct time' do
        expect(lap.time).to eq('23:49:08.277')
      end

      it 'has correct pilot' do
        expect(lap.pilot.id).to eq(38)
        expect(lap.pilot.name).to eq('F.MASSA')
      end

      it 'has correct lap number' do
        expect(lap.lap_number).to eq(1)
      end

      it 'has correct lap time' do
        expect(lap.lap_time).to eq(62.85)
      end

      it 'has correct average lap speed' do
        expect(lap.average_lap_speed).to eq(44.275)
      end
    end
  end
end
