require 'spec_helper'
include PilotFormatter

RSpec.describe PilotFormatter do
  describe '#format_pilots' do
    let(:input) do
      "Hora                               Piloto             Nº Volta   Tempo Volta       Velocidade média da volta\n" \
      "23:49:08.277      038 – F.MASSA                           1		1:02.852                        44,275\n" \
      "23:49:10.858      033 – R.BARRICHELLO                     1		1:04.352                        43,243\n" \
      "23:50:11.447      038 – F.MASSA                           2		1:03.170                        44,053\n" \
      "23:50:14.860      033 – R.BARRICHELLO                     2		1:04.002                        43,48"
    end
    let(:laps) { LapParser.new(input).parse }
    
    before do
      @best_lap, @pilots = PilotFormatter.format_pilots(laps)
    end

    context 'when formatting pilots' do
      let(:pilots) do
        pilots = {}
        laps.each do |lap|
          pilots[lap.pilot.id] = lap.pilot unless pilots[lap.pilot.id]
        end
        pilots
      end

      it 'has correct last lap' do
        pilots.values do |pilot|
          expect(pilot.last_lap).to eq(2)
        end
      end

      it 'has correct position' do
        expect(pilots[38].position).to eq(1)
        expect(pilots[33].position).to eq(2)
      end

      it 'has correct match time' do
        expect(pilots[38].match_time).to eq(126.02)
        expect(pilots[33].match_time).to eq(128.35)
      end

      it 'has correct best lap' do
        expect(@best_lap.lap_time).to eq(62.85)
      end

      it 'has correct best lap by pilot' do
        expect(pilots[38].best_lap).to eq(62.85)
        expect(pilots[33].best_lap).to eq(64.00)
      end

      it 'has correct average speed by pilot' do
        expect(pilots[38].average_speed).to eq(44.164)
        expect(pilots[33].average_speed).to eq(43.3615)
      end
    end
  end
end
