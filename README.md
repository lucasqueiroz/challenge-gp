# Gympass Challenge

This challenge proposes a log reader, where the input is a kart race log with the following information:

`Time     Pilot ID - Pilot name     Lap number     Lap time     Average lap speed'

The log is read from an input file, and returns a table containing the finishing position, pilot id, pilot name, completed laps and total race time for each pilot.

## Running

To run this project, you'll need to have Ruby installed.

1. Run `./lib/challenge.rb`

## Running tests

1. Run `rspec`

## Technologies

1. [Ruby](https://www.ruby-lang.org/en/)
2. [RSpec](https://rspec.info/) - Unit test framework
3. [Chronic Duration](https://github.com/henrypoydar/chronic_duration) - Implements a duration model into Ruby
4. [Terminal Table](https://github.com/tj/terminal-table) - Prints a formatted table
